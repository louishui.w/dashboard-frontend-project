import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { updateIsLoggedIn,logoutAppSlice } from "../../slice/appSlice";
import { Navigate, Link, Outlet, useNavigate } from "react-router-dom";
import { updateDashboardList, updateShareWithMeList, prepareData, deleteOneDashboard,logoutDashboardSlice } from "../../slice/dashboardSlice";
import style from "./dashboard.module.css";




function Nav(props) {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const isLoggedIn = useSelector((state) => state.appSlice.isLoggedIn);
    const dashboardList = useSelector((state) => state.dashboardSlice.dashboardList);
    const sharedWithMeList = useSelector((state) => state.dashboardSlice.sharedWithMeList);

    useEffect(() => {
        dispatch(prepareData())
    }, [])

    if (!isLoggedIn) {
        return <Navigate to={"/"} replace={true} />
    }



    let font = {
        "fontWeight": "800",
        "fontFamily": "monospace",
        "textDecoration": "underline"
    }

    let pointer = {
        "cursor": "pointer",
        "width": "max-content",
        "textDecoration": "none"
    }

    let hanldeOnClick = () => {
        console.log("hi, you just clicked me");
        //maybe not necessary
    }

    let handleLogoutClick = () => {
        localStorage.clear();
        dispatch(updateIsLoggedIn(false));
        dispatch(logoutAppSlice());
        dispatch(logoutDashboardSlice());
        // <Navigate to={"/"} replace={true}></Navigate>
    }

    //className={`${style.btn_customisation   }`}
    

    let handleButton = ()=>{
        console.log("hi");
        props.collaspeBtn();
    }

    return (
        <div className={`${style.navBarCanvas}`}>
            <div className={`${style.navBarWhole}`}>


                <div className={`${style.navBaritems_Wrapper}`}>

                    <div className={`${style.fontForIndividualItem}`}>
                        My Dashboard
                    </div>

                    <div >
                        {dashboardList.length !== 0 ? dashboardList.map(((item) =>
                            <div key={item._id} className={`${style.navBaritems}`} onClick={handleButton}>
                                <Link to={`${item._id}`} className={`${style.itemDashboard}`} >
                                    <div onClick={() => localStorage.setItem("dashboardName", item.dashboardName)}>{item.dashboardName}</div>
                                </Link>
                                <span className={`${style.navBarDeleteIcon}`} onClick={() => {
                                    console.log("hello is clicked");
                                    console.log(item._id);

                                    dispatch(deleteOneDashboard(item._id, navigate));

                                }}>␡</span>
                            </div>
                        )) : null}
                    </div>

                    <div className={`${style.fontForIndividualItem}`}>
                        Shared with me
                    </div>

                    <div>
                        {sharedWithMeList.map(((item) =>
                            <div className={`${style.navBaritems}`} onClick={handleButton}>
                                <Link to={`${item._id}`} className={`${style.itemDashboard}`} key={item._id} >
                                    <div onClick={() => localStorage.setItem("dashboardName", item.dashboardName)} >{item.dashboardName}</div>
                                </Link>
                            </div>
                        ))}
                    </div>
                </div>
               

                    <div className={`${style.createBtn_positional_padding}`} onClick={handleButton}>
                        <Link to="/dashboard/create">
                            <button className={`${style.btn_customisation   }`}>Create New Dashboard</button>
                        </Link>
                    </div>

                    <div className={`${style.logoutBtn_positional_padding}`}>
                        <button className={`${style.btn_customisation   }`} onClick={handleLogoutClick}>Logout</button>
                    </div>



            </div>

        </div>
    )
}

export default Nav;