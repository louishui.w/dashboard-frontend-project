import React, { useState, useEffect} from "react";
import { useSelector, useDispatch } from "react-redux";
import {updateIsLoggedIn, } from "../../slice/appSlice";
import { prepareData } from "../../slice/dashboardSlice";
import {Navigate,Link, Outlet} from "react-router-dom";
import Nav from "./navbar";
import style from "./dashboard.module.css"


function Dashboard(){
    const dispatch = useDispatch();
    const isLoggedIn = useSelector((state) => state.appSlice.isLoggedIn);
    const [username, setUsername] = useState(""); 
    const [hamburgerBtnIsClicked, setHamburgerBtnIsClicked] = useState(true);

    useEffect(()=>{
        setUsername(localStorage.getItem("username"));
        // dispatch(prepareData());

    })

    let handleOnClickOnHamburgerBtn= ()=>{
        console.log("hi");
        setHamburgerBtnIsClicked(state=>!state);
        
        
    }

  

    if (!isLoggedIn) {
        return <Navigate to={"/"} replace={true} />
    }
 
    // const hamburgerBtn = document.getElementsByClassName("hamburgerBtn")[0];
    // console.log(hamburgerBtn);


    console.log(hamburgerBtnIsClicked);

    return(
        <div className={`${style.margin}`}>

            <div className={`${style.navContainer}`}>

                <h1 className={`${style.welcome}`}>Welcome, {username}</h1>
                <a href="#" className={`${style.hamburgerBtn}`} onClick={handleOnClickOnHamburgerBtn}>
                    <span className={`${style.bar}`}></span>
                    <span className={`${style.bar}`}></span>
                    <span className={`${style.bar}`}></span>
                    
                </a>

                <div className={hamburgerBtnIsClicked? style.new_active : style.new}>
                {/* <div className={`${style.new_active}`}> */}
                {/* <div className={`${style.new}`}> */}
                    <Nav collaspeBtn={handleOnClickOnHamburgerBtn}/>
                </div>
                
            </div>

            <Outlet/>
        </div>
    )
}

export default Dashboard;