import React from "react";
import { BrowserRouter, 
    Routes,
    Route,
    Link,
    useParams,
    useOutlet,
    Navigate,
    Outlet,
    useLocation } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";


function Protected(props){
    const isLoggedIn = useSelector((state) => state.appSlice.isLoggedIn);

    return(
        <div>
            {isLoggedIn? props.children:<Navigate to={"/"} replace={true}/>}
        </div>
    )


}

export default Protected