import React, { useEffect } from "react";
import { updateShareWithOtherListEmpty } from "../../slice/dashboardSlice";
import { useSelector, useDispatch } from "react-redux";
import { Navigate, Link, Outlet } from "react-router-dom";
import {
    prepareListForNameSearch,
    updateSearchInput,
    updateShareWithOtherList,
    updateNameForDashboard,
    createDashboard,
    updateErrorMsgNoName,
    dashboardSlice
} from "../../slice/dashboardSlice";
// import style from "./dashboard.module.css";
import style from "../Dasbboard/dashboard.module.css"




function CreatedNewDashboard() {
    const dispatch = useDispatch();
    const memberList = useSelector((state) => state.dashboardSlice.memberList);
    const searchInput = useSelector((state) => state.dashboardSlice.searchInput);
    const shareWithOtherList = useSelector((state) => state.dashboardSlice.shareWithOtherList);
    const nameForDashboard = useSelector((state) => state.dashboardSlice.nameForDashboard);
    const errorMsgNoName = useSelector((state) => state.dashboardSlice.errorMsgNoName);
    const createDashboardSuccess = useSelector((state) => state.dashboardSlice.createDashboardSuccess)

    console.log("line 18", memberList);
    console.log("line 19", shareWithOtherList);

    useEffect(() => {
        console.log("useEffect triggered!");
        dispatch(prepareListForNameSearch());
    }, [])

    let handleChange = (e) => {
        console.log("handleChange");
        dispatch(updateSearchInput(e.target.value));
    }

    let handleClick = (item) => {
        console.log("clicked, clicked")
        // console.log(item);
        dispatch(updateShareWithOtherList(item));
        dispatch(updateSearchInput(""));
    }

    let handleDashboardName = (e) => {
        dispatch(updateNameForDashboard(e.target.value))
    }

    let handleConfirm = () => {
        if (nameForDashboard === "") {
            console.log("please enter a name");
            dispatch(updateErrorMsgNoName(true));
            return
        }
        dispatch(createDashboard());
        dispatch(updateErrorMsgNoName(false));
        dispatch(updateShareWithOtherListEmpty([]));
    }

    if (createDashboardSuccess) {
        return <Navigate to={"/dashboard"} />
    }

    const searchResult = memberList.filter((item) => {
        // console.log(item);
        // console.log(item.memberName)
        // console.log(item.memberName.toLowerCase())

        if (item.memberName.toLowerCase().indexOf(searchInput.toLowerCase()) >= 0 && item.memberName !== localStorage.getItem("username")) {
            return true;
        } else {
            return false;
        }
    })

    let errorMsg = {
        "fontSize": "xx-small",
        "color": "red"
    }

    let smallerFont = {
        
        "fontStyle": "italic",
        "fontSize": "1rem",
        "fontFamily": "Quicksand"
    }
    return (
        <div className={`${style.createDashboard_Wrapper}`}>
            <div>


                <div>
                    <h2 className={`${style.createDashboard_title}`}> Create Dashboard</h2>
                </div>

                <div>
                    <div className={`${style.createDashboard_subtitle}`}>Dashboard Name:</div>
                    <input placeholder="enter here" onChange={handleDashboardName} value={nameForDashboard} />
                </div>
                {errorMsgNoName ? <span style={errorMsg}>Please enter a name</span> : null}


                <div>
                    <div className={`${style.createDashboard_subtitle}`}>Share With:</div>
                    <div>
                        <input placeholder="type here to search" onChange={handleChange} value={searchInput} />
                    </div>
                    {searchInput !== "" ? searchResult.map((item) => <div style={{ "cursor": "pointer" }} key={item._id} onClick={() => handleClick(item)}>{item.memberName}</div>) : null}
                    {shareWithOtherList.length > 0 ? <div style={smallerFont}>You have selected:</div> : null}
                    {shareWithOtherList.map((item) => (<div key={item._id}>{item.memberName}</div>))}
                </div>

                <button onClick={handleConfirm} className={`${style.createBtn}`}>
                    +create
                </button>



                <div>
                    <Link to="/dashboard" className={`${style.back}`}>Back</Link>
                </div>

            </div>
        </div>
    )
}

export default CreatedNewDashboard;