import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { updateIsLoggedIn } from "../../slice/appSlice";
import { useNavigate } from "react-router-dom";
import { fetchGetChart, updateBackToDashboard } from "../../slice/chartSlice";
import styleOfButton from "../../components/btn.module.css"
import BarChart from "../Charts/BarChart"
import LineChart from "../Charts/LineChart"
import PieChart from "../Charts/PieChart"
import styleAllChart from "./chart.module.css"
import {
    BrowserRouter,
    Routes,
    Route,
    Link,
    useParams,
    useOutlet,
    Navigate,
    Outlet,
    useLocation
} from "react-router-dom";




function Charts() {
    let { dashboardId } = useParams()
    const data = useSelector((state) => state.chartSlice.chartData);
    const dashboardList = useSelector((state) => state.dashboardSlice.dashboardList)
    const sharedWithMeList = useSelector((state) => state.dashboardSlice.sharedWithMeList)
    const navigate = useNavigate();

    console.log("herer", dashboardList)
    let dispatch = useDispatch()

    useEffect(() => {
        dispatch(fetchGetChart(dashboardId, navigate));

    }, [dashboardId])


    let dashboardName;
    let dashboardToken;

    if (dashboardList.length > 0 || sharedWithMeList.length > 0) {
        let dashboard = dashboardList.find((item) => {
            return item._id === dashboardId
        })
        let shareWithMeDashboard = sharedWithMeList.find((item) => {
            return item._id === dashboardId
        })


        console.log(dashboard)
        console.log(shareWithMeDashboard)


        if (dashboard) {
            dashboardName = dashboard.dashboardName

            dashboardToken = dashboard.token
            console.log(`dashboard.token`, dashboardToken)
        }

        if (shareWithMeDashboard) {
            dashboardName = shareWithMeDashboard.dashboardName;
            dashboardToken = shareWithMeDashboard.token
        }
    }

 



    return (
        <>
            <div className={styleAllChart.chartsWrapper} >

                <div className={styleAllChart.flex}>
                    <div className={`${styleAllChart.fontDasboardName}`}>dashboard: {dashboardName}</div><div className={`${styleAllChart.fontDashboardToken}`}>Dashboard Token : {dashboardToken}</div>
                    <Link to={`/dashboard/${dashboardId}/create-chart`}><button className={`${styleOfButton.btn_1}`}>create Chart</button></Link>
                </div>

                <div className={`${styleAllChart.oneChartWrapper} `}>
                    {data.length > 0 ? data.map(item => {
                        if (item.chartType === "bar") {
                            return <BarChart dashboardToken={dashboardToken}key={item._id} id={item._id} title={item.chartName} chartType={item.chartType} dataSet={item.dataSet} />
                        }
                        else if (item.chartType === "line") {
                            return <LineChart dashboardToken={dashboardToken} key={item._id} id={item._id} dataSet={item.dataSet} chartType={item.chartType} chartName={item.chartName} />
                        } else {
                            return <PieChart dashboardToken={dashboardToken} key={item._id} id={item._id} title={item.chartName} chartType={item.chartType} dataSet={item.dataSet} />
                        }
                    }
                    ) : null}
                </div>
            </div>
        </>
    )
}
export default Charts;