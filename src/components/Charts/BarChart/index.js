// render <div>Bar chart</div>
import React from 'react';
import { useEffect, useState } from "react";
import { BrowserRouter, Routes, Route, useParams, useLocation, Link, Outlet, Navigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';
import backgroundColor from '../PieChart/backgroudcolor'
import borderColor from '../PieChart/borderColor'
import { fetchGetChartData, fetchDeleteChart } from '../../../slice/chartSlice';
import styleOfButton from "../../btn.module.css"
import styleAllChart from "../chart.module.css"

ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
);

function BarChart(props) {
    const dispatch = useDispatch();
    const location = useLocation();
    const dashboardId = useParams();
    const [sample1, setSample1] = useState(false);


    useEffect(() => {

        const interval = setInterval(() => {
            dispatch(fetchGetChartData(props.id))

        }, 5000);
        return () => {
            clearInterval(interval);
        }
    }, [])

    const options = {
        responsive: true,
        plugins: {
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: props.title, // should get one more props as chart title
            },
        },
        animation: false,
        aspectRatio:1
    };



    const labels = props.dataSet.x
    // ['January', 'February', 'March', 'April', 'May', 'June', 'July'];

    const data = {
        labels,
        datasets: listOfThing()
    };

    function listOfThing() {
        let arr = [];

        for (let i = 0; i < props.dataSet.data.length; i++) {

            arr.push({
                label: props.dataSet.data[i].name,
                data: props.dataSet.data[i].data,
                backgroundColor: borderColor[i+2]
                // randomColor(),
            })
        }
        return arr
    }


    let handleDisplaySample = () => {
        setSample1(value => {
            return !value
        })
    }

    const handleDeleteChart = () => {
        console.log(dashboardId)
        dispatch(fetchDeleteChart(props.id, dashboardId))
    }

    let host = window.location.host

    //{sampleCode}
    let sample =
    {
        "x": ["a", "b", "c", "d"],
        "data": [
            {
                "name":"label",
                "data": [5,10,15,20]
        }],
        "token":props.dashboardToken
        
        // "mode": "insert",
        // "x": ["Jan", "Feb", "Mar", "Apr", "May"],
        // "name": "test label A",
        // "newData": [10, 20, 43, 11, 25]
    }


    const copy = () => {
        /* Copy the text inside the text field */
        navigator.clipboard.writeText(JSON.stringify(sample, null, 4));

    }
    
    let endPoint = `${host}/charts/${props.id}/${props.chartType}`
    function copyEndPoint() {
      
        /* Copy the text inside the text field */
        navigator.clipboard.writeText(endPoint);
        
      }
    return (
        <div   className={`${styleAllChart.chartStyle}`}>
            <div className={`${styleAllChart.smapleAndDeleteApp}`}>

            <button  className={`${styleOfButton.btn_1}`}  onClick={handleDisplaySample}>JSON Sample</button>
            <button className={`${styleOfButton.btn_1}`} onClick={handleDeleteChart}>Delete Chart </button>
            <button className={`${styleOfButton.btn_1} ${styleAllChart.endPointApp}`} onClick={copyEndPoint}>Copy EndPoint</button>
            </div>

            {sample1 ? <div><pre>{JSON.stringify(sample, null, 4)}</pre></div> : null}
            {sample1 ? <button className={`${styleOfButton.btn_1}`} onClick={copy}>copy simple</button> : null}

            <div className={`${styleAllChart.endPointApp}`}> PATCH API : {endPoint} </div> 
            
            <Bar options={options} data={data} />

        </div>
    )
}


export default BarChart
