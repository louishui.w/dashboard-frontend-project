import React from 'react';
import { useSelector, useDispatch } from "react-redux";
import { useParams } from 'react-router-dom';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import { Line } from 'react-chartjs-2';
import { useEffect, useState } from "react";
import { fetchGetChartData, fetchDeleteChart, fetchGetChart } from '../../../slice/chartSlice';
import style from "../LineChart/lineStyle.module.css";
import borderColor from '../PieChart/borderColor';
import styleAllChart from "../chart.module.css"
import styleOfButton from "../../btn.module.css"


ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);

function LineChart(props) {

    const dispatch = useDispatch();
    const [sample1, setSample1] = useState(false);

    let { dashboardId } = useParams()


    useEffect(() => {

        const interval = setInterval(() => {
            dispatch(fetchGetChartData(props.id))

        }, 5000);
        return () => {
            clearInterval(interval);
        }
    }, []);


    const options = {
        responsive: true,
        plugins: {
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: props.chartName,
            },

        },
        animation: false,
        aspectRatio:1
        // maintainAspectRatio: false
    };

    const labels = props.dataSet.x


    function listOfThing() {
        let arr = [];

        for (let i = 0; i < props.dataSet.data.length; i++) {


            // console.log(props.dataSet.data[i].data);
            arr.push({
                label: props.dataSet.data[i].name,
                //  props.dataSet.nameOfLine ,
                data: props.dataSet.data[i].data,
                borderColor: borderColor[i+2],
                // backgroundColor: 'rgba(255, 99, 132, 0.5)',
                backgroundColor: borderColor[i+2]
            })

        }

        return arr
    }

    const data = {
        labels,
        datasets: listOfThing()


    };

    // const data = {
    //         labels,
    //         datasets: [

    //                 {
    //                         label: 'Dataset 1',
    //                         data: [13,21,32,45],
    //                         borderColor: 'rgb(255, 99, 132)',
    //                         backgroundColor: 'rgba(255, 99, 132, 0.5)',
    //                 },
    //                 {
    //                         label: 'Dataset 2',
    //                         data: [10, 22, 33, 40, 50],
    //                         borderColor: 'rgb(53, 162, 235)',
    //                         backgroundColor: 'rgba(53, 162, 235, 0.5)',
    //                 },
    //         ],
    // };


    // useEffect(
    //         // setInterval(() => {
    //         //         dispatch(fetchGetChartData(props.id))
    //         // }, 5000)
    //         // ***clear timer
    //         , [])



    let host = window.location.host
    // let host  = "localhost:8000"
    let sample =
    {
        "x": ["item1","item2"],
     
        "data": [
            {
                "name": "line1", 
                "data": [99,221]
            },
            {
                "name": "line2",
                "data": [221,99]
            },
            
        ],
        "token": props.dashboardToken
    }
    const copy = () => {
        /* Copy the text inside the text field */
        navigator.clipboard.writeText(JSON.stringify(sample, null, 4));

    }
    const handleDisplaySample = () => {
        // setSample1(true)
        setSample1(value => {
            return !value
        })
    }
    const handleDeleteChart = () => {
        console.log(dashboardId)
        dispatch(fetchDeleteChart(props.id, dashboardId))

    }
    let endPoint = `${host}/charts/${props.id}/${props.chartType}`
    function copyEndPoint() {
      
        /* Copy the text inside the text field */
        navigator.clipboard.writeText(endPoint);
        
      }
   
    return (
        <div  className={`${styleAllChart.chartStyle}`}>
         
         <div className={`${styleAllChart.smapleAndDeleteApp}`}>

            <button className={`${styleOfButton.btn_1}`} onClick={handleDisplaySample} >JSON Sample</button>
            <button className={`${styleOfButton.btn_1}`} onClick={handleDeleteChart}>Delete Chart </button>
            <button className={`${styleOfButton.btn_1} ${styleAllChart.endPointApp}`} onClick={copyEndPoint}>Copy EndPoint</button>
        </div>   
            {sample1 ? <div><pre>{JSON.stringify(sample, null, 4)}</pre></div> : null}
            {sample1 ? <button className={`${styleOfButton.btn_1}`} onClick={copy}>copy simple</button> : null}



            <div className={`${styleAllChart.endPointApp}`}> PATCH API : {endPoint}</div> 
            <Line options={options} data={data} />

        </div>
    )
}


export default LineChart
