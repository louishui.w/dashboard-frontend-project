import React from 'react';
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Pie } from 'react-chartjs-2';
import { fetchGetChartData, fetchDeleteChart } from '../../../slice/chartSlice';
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import styleAllChart from "../chart.module.css"
import styleOfButton from "../../btn.module.css"
import { useSelector, useDispatch } from "react-redux";
import backgroundColor from "./backgroudcolor"
ChartJS.register(ArcElement, Tooltip, Legend);



function PieChart(props) {
    const dispatch = useDispatch();
    const dashboardId = useParams();
    const [sample1, setSample1] = useState(false);

    useEffect(() => {

        const interval = setInterval(() => {
            dispatch(fetchGetChartData(props.id))

        }, 5000);
        return () => {
            clearInterval(interval);
        }
    }
        , []);

    // console.log(props.dataSet)

    const handleDeleteChart = () => {
        console.log(dashboardId)
        dispatch(fetchDeleteChart(props.id, dashboardId))
    }

    const data = {
        ///////////////////
        labels: props.dataSet.category,
        //  ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        /////////////////
        datasets: [
            {
                label: '# of Votes',
                data: props.dataSet.data,

                //dispatc
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                ]
                ,
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                ]
                ,
                borderWidth: 10,
            },
        ],


    };

    const options = {
        responsive: true,
        plugins: {
            title: {
                display: true,
                text: props.title
            }
        },
        animation: false,
        aspectRatio:1
    }

    let handleDisplaySample = () => {
        setSample1(value => {
            return !value
        })
    }
    let sample =
    { 
        "category":["a", "b", "c", "d"],
        "data":[5,4,5,6],
        "token": props.dashboardToken
    }

    const copy = () => {
        /* Copy the text inside the text field */
        navigator.clipboard.writeText(JSON.stringify(sample, null, 4));

    }
    // console.log(props.dataSet);
    // console.log(props.dataSet.category);
    // console.log(props.dataSet.data);
    // console.log(props.title);
    // console.log(props.dataset)
    let host = window.location.host
    let endPoint = `${host}/charts/${props.id}/${props.chartType}`
    function copyEndPoint() {
      
        /* Copy the text inside the text field */
        navigator.clipboard.writeText(endPoint);
        
      }
    return (
        <div className={`${styleAllChart.chartStyle}`}>

        <div className={`${styleAllChart.smapleAndDeleteApp}`}>
            <button className={`${styleOfButton.btn_1}`}  onClick={handleDisplaySample}>JSON Sample</button>
            <button className={`${styleOfButton.btn_1}`} onClick={handleDeleteChart}>Delete Chart </button>
            <button className={`${styleOfButton.btn_1} ${styleAllChart.endPointApp}`} onClick={copyEndPoint}>Copy EndPoint</button>
        </div>
            {sample1 ? <div><pre>{JSON.stringify(sample, null, 4)}</pre></div> : null}
            {sample1 ? <button className={`${styleOfButton.btn_1}`}  onClick={copy}>copy simple</button> : null}

            <div className={`${styleAllChart.endPointApp}`}> PATCH API : {endPoint} </div> 
            <Pie data={data} options={options}  />


        </div>
    )
}


export default PieChart
