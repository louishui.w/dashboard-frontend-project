import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { updateIsLoggedIn } from "../../slice/appSlice";
import { useNavigate, Link, Outlet, useParams } from "react-router-dom";

import styleOfButton from "../../components/btn.module.css"
import style from "./createChart.module.css"

import { fetchCreateChart, updateMsg } from "../../slice/chartSlice"
import barChart from "./bar-chart.png"
import lineChart from "./line-chart.png"
import pieChart from "./ratio.png"

function CreateChart() {
    const msg = useSelector(function (state) {
        return state.chartSlice.msg
    })
    const dispatch = useDispatch();
    const navigate = useNavigate()
    const { dashboardId } = useParams()
    console.log(dashboardId)

    const [inputChartName, setinputDashboardName] = useState("")
    const [chartType, setChartType] = useState("")
    const [pie, setPie] = useState("")
    useEffect(() => {
        dispatch(updateMsg(false))
    }, [])

    const handleOnChangeChartType = (e) => {
        setChartType(e.target.value)
    }
    const handleCreateOnCLick = () => {
        if (inputChartName === "" || chartType === "") {
            return alert("input ChartType && dashBoard Name!!")
        }
        dispatch(fetchCreateChart(inputChartName, chartType, dashboardId, navigate))
        setinputDashboardName("")
        //think here  , collect data send to redux , use fetch server!!!
    }
    const handleOnchangeChartName=(e)=>{
        setinputDashboardName(e.target.value)
    }
    const handleOnclickChartType = (chartType)=>{
        console.log(chartType);
        setChartType(chartType);
          
    }
    let barChartClass = style.chartType;
    let lineChartClass = style.chartType;
    let pieChartClass = style.chartType;
    if(chartType === "bar"){
        barChartClass += style.chartSelected;
    }else if(chartType === "line"){
        lineChartClass += style.chartSelected;
    }else if(chartType === "pie"){
        pieChartClass += style.chartSelected;
    }
    console.log(pieChartClass)
    
        return (
                <>
                        <div className={`${style.flexColumn} ${style.setPosition}`}>
                            <Link to="/dashboard">back</Link>


                            <div className={`${style.flexRow} ${style.setImgPostition}`}>

                            
                                    <div  className={barChartClass} onClick={()=>{handleOnclickChartType("bar")}} ><img  className={style.chartImg} src={barChart}></img></div>
                                    <div  className={lineChartClass} onClick={()=>{handleOnclickChartType("line")}} ><img  className={style.chartImg}  src={lineChart}></img></div>
                                    <div  className={pieChartClass} onClick={()=>{handleOnclickChartType("pie")}} ><img   className={style.chartImg} src={pieChart}></img></div>


                                
                                    {/* <input onChange={handleOnChangeChartType} type="radio" id="lineChart" name="chart" value="line" />
                                    <img className={style.chartImg} src={lineChart} alt="lineChart" />
                                    <br />
                                    <input onChange={handleOnChangeChartType} type="radio" id="lineChart" name="chart" value="pie" />
                                    <img className={style.chartImg} src={pieChart} alt="pieChart" /> */}
                            </div>
                            <div className={style.inputDashboardName} >
                                    <input type="text" className={style.chartName} onChange={handleOnchangeChartName} value={inputChartName} placeholder="ChartName" />
                                    {msg ? <span>success</span> : null}
                            </div>
                            <div className={style.createBtn}>
                                    <button className={styleOfButton.btn_1} onClick={handleCreateOnCLick}>Create</button>
                                    {/* <Link to= {`/dashboard/${dashboardId}`}>GOBACK</Link> */}
                            </div>


                        </div>
                </>
        )
}

export default CreateChart;