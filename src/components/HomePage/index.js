import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { toLogin, updateUserIdLogin, updatePasswordLogin, updateRegisterFail} from "../../slice/appSlice";
import { fetchRegister } from "../../slice/appSlice";
import { Navigate} from "react-router-dom";
import style from  "./homePage.module.css";
import styleOfButton from "../../components/btn.module.css"


let font = {
    "fontWeight": "800",
    "fontFamily": "Quicksand"
}
let red = {
    "color": "red",
    "fontWeight": "800",
    "fontFamily": "Quicksand"
}


function HomePage() {

    return (
        <div className={`${style.LoginRegisterMargin} ${style.flex}`}>
   
                <Login></Login>
                <Register></Register>
   
        </div>

    )
}

function Login() {
    const dispatch = useDispatch();
    const isLoggedIn = useSelector((state) => state.appSlice.isLoggedIn);
    const userIdLogin = useSelector((state) => state.appSlice.userIdLogin);
    const passwordLogin = useSelector((state) => state.appSlice.passwordLogin);
    const loginFail = useSelector((state) => state.appSlice.loginFail);


    // const [loginId, setLoginId] = useState("");
    // const [loginPassword, setLoginPassword] = useState("");
    const [missingLoginId, setMissingLoginId] = useState(false);
    const [missingLoginPassword, setMissingLoginPassword] = useState(false);


    let handleLoginClick = () => {
        console.log("login is clicked");

        if (userIdLogin === "") {
            setMissingLoginId(true);
            return;
        } else {
            setMissingLoginId(false);
        }

        if (passwordLogin !== "") {
            setMissingLoginPassword(false);
        } else {
            setMissingLoginPassword(true);
            return;
        }
        dispatch(toLogin());
    }

    if (isLoggedIn) {
        return <Navigate to={"/dashboard"} replace={true} />
    }

    let handleOnKeyDownLogin= (e)=>{
        if(e.key === "Enter"){
            console.log("enter is pressed");
            // handleOnClick();
            anotherPostMethodLogin();
        }
    }
    let anotherPostMethodLogin = ()=> {handleLoginClick()};


  

    return (
        <div className={`${style.shell} ${style.shellBackgroundColor}`}>
            <h1> Login</h1>

            <div>
                <input placeholder="loginID"
                value={userIdLogin}
                onChange={(e) => { dispatch(updateUserIdLogin(e.target.value))}} />
            </div>

            {missingLoginId ? <span style={red}>please enter login Id</span> : null}

            <div><input type="password" placeholder="password" value={passwordLogin} onKeyDown={handleOnKeyDownLogin} onChange={(e) => { dispatch(updatePasswordLogin(e.target.value)) }} />
            </div>

            {missingLoginPassword ? <span style={red}>please enter password</span> : null}
            {loginFail ? <span style={red}>login unsuccessful</span> : null}

            <div className={`${styleOfButton.flex}`}>
                <button  className={`${styleOfButton.btn_1}`} onClick={handleLoginClick}><span>Login</span></button>
            </div>

        </div>
    )
}





function Register() {
    const isLoggedIn = useSelector(function (state) {
        return state.appSlice.isLoggedIn
    });
    const registerFail = useSelector((state) => state.appSlice.registerFail);

    const dispatch = useDispatch()
    const [inputRegisterId, setRegisterId] = useState("");
    const [inputRegisterPw, setRegisterPw] = useState("");
    const [missingRegId, setMissingReginId] = useState(false);
    const [missingRegPassword, setMissingRegPassword] = useState(false);

    const handleOnChangeRegisterId = (e) => {
        setRegisterId(e.target.value)
    }

    const handleOnChangeRegisterPw = (e) => {
        setRegisterPw(e.target.value)
    }


    const handleRegister = () => {

        if (inputRegisterId === "") {
            setMissingReginId(true);
            return;
        } else {
            setMissingReginId(false);
        }

        if (inputRegisterPw !== "") {
            setMissingRegPassword(false);
        } else {
            setMissingRegPassword(true);
            return;
        }
        // if(inputRegisterId==="" ||inputRegisterPw === "" ){
        //     // alert ("Please input both register ID & password")
        //     return 
        // }
        dispatch(fetchRegister(inputRegisterId, inputRegisterPw))
    }


    let anotherPostMethodRegister = ()=>{handleRegister()};


    let handleOnKeyDownRegister = (e)=>{
        if(e.key === "Enter"){
            console.log("enter is pressed");
            // handleOnClick();
            anotherPostMethodRegister();
        }
    }
    // console.log(isLoggedIn)
    if (isLoggedIn) {
        return <Navigate to={"/dashboard"} replace={true} />
    }

    return (
        <div className={`${style.shell} ${style.shellBackgroundColor}`}>
            <h1> Register</h1>
            <div>
                <input type="text" value={inputRegisterId} placeholder="RegisterID" onChange={handleOnChangeRegisterId}></input>
            </div>
            {missingRegId ? <span style={red}>please enter register Id</span> : null}
            <div>
                <input type="password" value={inputRegisterPw} placeholder="password" onKeyDown={handleOnKeyDownRegister} onChange={handleOnChangeRegisterPw}></input>
            </div>
            {missingRegPassword ? <span style={red}>please enter password</span> : null}


            {registerFail ? <span style={red}>userId is taken</span> : null}

            <div className={`${styleOfButton.flex}`}>
                <button className={`${styleOfButton.btn_1}`} onClick={handleRegister}>Register</button>
            </div>
        </div>
    )
}

export default HomePage;
