import { configureStore } from "@reduxjs/toolkit";
import appSlice from "../slice/appSlice";
import dashboardSlice from "../slice/dashboardSlice";
import chartSlice from "../slice/chartSlice";



export default configureStore({
    reducer: {
        appSlice: appSlice,
        dashboardSlice :dashboardSlice,
        chartSlice: chartSlice
    }
});