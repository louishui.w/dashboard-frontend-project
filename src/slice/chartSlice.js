import { createSlice } from "@reduxjs/toolkit";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";


const initialState = {
    isLoggedIn: localStorage.getItem("token") ? true : false,
    msg: false,
    chartData: [],    
    backToDashboard: "",
   
}

export const appSlice = createSlice({
    name: "app",
    initialState: initialState,
    reducers: {
        updateIsLoggedIn: function (state, action) {
            state.isLoggedIn = action.payload
        },
        updateMsg: function (state, action) {
            state.msg = action.payload
        },
        updateChartData: function (state, action) {
            state.chartData = action.payload
        },
        updateDeleteOneChart: function(state, action){
            state.chartData= state.chartData.filter(item=>{
                return item._id !== action.payload
            })
        },
        updateChartDataSet: function (state, action) {
           
            let chartTarget = state.chartData.find((item) => {
                return item._id === action.payload._id
            })

            if (chartTarget) {
                chartTarget.dataSet = action.payload.dataSet
            }
        },
        updateBackToDashboard: function(state, action){
            state.backToDashboard = action.payload
        },
  
    }
})
function handleAuth() {

    const token = localStorage.getItem("token");

    const forHeader = "Bearer ".concat(token);

    return forHeader;

}
export const {
    updateUserIdLogin,
    updateMsg,
    updateChartData,
    updateChartDataSet,
    updateBackToDashboard,
    updateEmptyMsg,
    updateDeleteOneChart
} = appSlice.actions;

export const fetchDeleteChart =function (chartId,dashboardId){
    return async (dispatch ,getState)=>{
        const authHearder = handleAuth();

        console.log("chartId", chartId)

        
        console.log("dashboardId",dashboardId)

        let postData = {
        
            headers: {
                'content-type': 'application/json',
                'authorization': authHearder
            },
            method: "DELETE"
        }

        const res = await fetch(`/charts/${chartId}`, postData)
        const data = await res.json()

        if(data.success){

            dispatch(updateDeleteOneChart(chartId));

        }
        console.log(data);
    }
}

export const fetchCreateChart = function (chartName, chartType,dashboardId,navigate) {
    return async (dispatch, getState) => {
        // console.log("create Chart")
       
        const authHearder = handleAuth();

        let obj = {
          
        };

        if (chartName) {
            obj.chartName = chartName

        }
        if (chartType) {
            obj.chartType = chartType
        }

        let postData = {
            body: JSON.stringify(obj),
            headers: {
                'content-type': 'application/json',
                'authorization': authHearder
            },
            method: "POST"
        }

        const res = await fetch(`/dashboard/${dashboardId}/chart`, postData)
        console.log("line 127", res);
        const data = await res.json()

        console.log(data)

        if (data.success) {
            dispatch(updateMsg(true))
            navigate(`/dashboard/${dashboardId}`,{replace:true})
        } else {
            alert("false")
            dispatch(updateMsg(false))

        }

    }
}



export const fetchGetChart = function (dashboardId, navigate) {

    return async (dispatch, getState) => {
        // console.log("get Chart")
        const authHearder = handleAuth();

        let postData = {

            headers: {
                'authorization': authHearder
            },
            method: "GET"
        }

        const res = await fetch(`/dashboard/${dashboardId}/chart`, postData)
        const data = await res.json()


        console.log("chartSlice line 165",data);
        // if (data && data.length > 0) {
        // if(data=== []){
        //     console.log("this user has no access to this dashboard");
        //     // navigate(`/dashboard/`,{replace:true});
        // }
        dispatch(updateChartData(data))

        // } else {
        //     // alert("this dashboard  not have chart ")
        //     console.log("no chart got")
        // }

    }
}
//useEffect PieChart Folder index.js 
export const fetchGetChartData = function (chartId) {


    return async (dispatch, getState) => {

        const authHearder = handleAuth();
 
        let postData = {

            headers: {
                'authorization': authHearder
            },
            method: "GET"
        }
      


        const res = await fetch(`/charts/${chartId}`, postData);
        // console.log(res)

        const data = await res.json();
     
        console.log(data)
        if (data.length>0) {
            dispatch(updateChartDataSet(data[0]))
        }

    }
}





export default appSlice.reducer;
