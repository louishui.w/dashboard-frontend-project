import { createSlice } from "@reduxjs/toolkit";
import {fetchGetChart} from "./chartSlice";
import { useNavigate } from "react-router-dom";


const initialState = {
    dashboardList:[],
    sharedWithMeList:[],
    searchInput:"",
    memberList:[],
    shareWithOtherList:[],
    nameForDashboard: "",
    errorMsgCreateNewDashboard: false,
    errorMsgNoName: false,
    createDashboardSuccess:false

}

export const dashboardSlice = createSlice({
    name: "dashboard",
    initialState: initialState,
    reducers:{
        logoutDashboardSlice: function(state, action){
            state = {...initialState};
        },
        updateDashboardList: function(state, action){
            state.dashboardList = action.payload
        },
        /////////////////////🔴
        updateDeleteOneDashboard: function(state, action){
            console.log("insde updateDeleteOneDashboard",action.paylaod);
            state.dashboardList= state.dashboardList.filter(item=>{
                return item._id !== action.payload
            })
        },
        //////////////////////🔴
        updateShareWithMeList: function (state, action){
            state.sharedWithMeList = action.payload
        },
        updateMemberList: function(state, action){
            state.memberList = action.payload
        },
        updateSearchInput: function(state, action){
            state.searchInput = action.payload
        },
        updateShareWithOtherList: function(state, action){
            // state.shareWithOtherList = [ ...state.shareWithOtherList, action.payload ];
            state.shareWithOtherList.push(action.payload);            
        },
        updateShareWithOtherListEmpty: function(state, action){
            // state.shareWithOtherList = [ ...state.shareWithOtherList, action.payload ];
            state.shareWithOtherList= action.payload;            
        },

        updateNameForDashboard: function(state, action){
            state.nameForDashboard = action.payload
        },
        updateErrorMsgCreateNewDashboard: function (state, action){
            state.errorMsgCreateNewDashboard = action.payload
        },
        updateErrorMsgNoName: function(state, action){
            state.errorMsgNoName = action.payload
        },
        updateCreateDashboardSuccess: function(state, action){
            state.createDashboardSuccess = action.payload
        }
        
    }
})

/// testing: prototype for dealing with the header => seems to be working
function handleAuth (){

    const token = localStorage.getItem("token");
 
    const forHeader = "Bearer ".concat(token);
 
    return forHeader;

}
/// testing: prototype for dealing with the header


export const prepareListForNameSearch = function(){
    return(dispatch, getState)=>{
        const settingHeader = handleAuth();
        console.log("settingHeader", settingHeader);
    
        fetch("/dashboard/memberList", {
            headers: {
                // 'content-type': 'application/json',
                'Authorization': settingHeader
            },
            method: 'GET'
        }).then(res=>res.json()).then((data)=>{
         
            dispatch(updateMemberList(data.result));
        })
    }
}


export const deleteOneDashboard = function(dashboardId,navigate ){
    return(dispatch, getState)=>{
        const settingHeader = handleAuth();

        fetch(`/dashboard/${dashboardId}`,{
            headers: {
                'Authorization': settingHeader
            },
            method: 'DELETE'
        }).then(res=>res.json()).then((data)=>{
            console.log("frontend lin 107, fetching delete requst",data);
            if(data.success){
                console.log("see if success === true... line 109");
                dispatch(updateDeleteOneDashboard(dashboardId));
                // dispatch(fetchGetChart(dashboardId));
                navigate(`/dashboard/`,{replace:true})
            }
        })

    }

}
export const prepareData = function(){
    return (dispatch, getState)=>{

        const settingHeader = handleAuth();

        fetch("/dashboard", {
            headers: {
                'Authorization': settingHeader
            },
            method: 'GET'
        }).then(res=>res.json()).then((data)=>{

            if(data.success){
      
                if(data.dashboard.length >0){        

                    const listOfdashboard = data.dashboard.filter((item)=>{
                        return item.ownerName === localStorage.getItem("username")
                    })
                    // console.log(listOfdashboard);
/////////////////
                    if(listOfdashboard){
                        // console.log(listOfdashboard);
                        dispatch(updateDashboardList(listOfdashboard));
                    }
/////////////////
                    const listOdDashboardSharedWithMe = data.dashboard.filter((item)=>{
                        return item.ownerName !== localStorage.getItem("username")
                    })
                    // if(listOdDashboardSharedWithMe.length>0){
                        dispatch(updateShareWithMeList(listOdDashboardSharedWithMe));
                    // }

                    dispatch(updateCreateDashboardSuccess(false));
                    dispatch(updateNameForDashboard(""));
                    // dispatch(updateShareWithOtherListEmpty([]));


                }else if(data.dashboard.length === 0){
                    console.log("there is nothing yet");
                    console.log(getState().dashboardSlice.shareWithOtherList)
                    console.log(getState().dashboardSlice.sharedWithMeList)
                    dispatch(updateDashboardList([]));
                    dispatch(updateShareWithMeList([]));
                    // dispatch(updateShareWithOtherListEmpty([]));


                    return
                }

                // console.log(data.shareWithMemberList);

            }else{
                console.log("not found in dataBase");
                return
            }
        })
    }
}

export const createDashboard = function(){
    return(dispatch, getState)=>{
        console.log("hi");
        const settingHeader = handleAuth();
        // console.log(getState.appSlice);
        let info= {
            dashboardName: getState().dashboardSlice.nameForDashboard,
            shareWith: getState().dashboardSlice.shareWithOtherList
        }

        console.log("133 info:",info);

        
        fetch("/dashboard", {
            body: JSON.stringify(info),
            headers: {
                'content-type': 'application/json',
                'Authorization': settingHeader
            },
            method: 'POST'
        }).then((res) => { return res.json() }).then((resData) => {
            console.log(resData);
            if (resData.success) {
                
                dispatch(updateErrorMsgCreateNewDashboard(false));
                dispatch(updateCreateDashboardSuccess(true));
                dispatch(prepareData());
                alert("dashboard cretaed!");

            } else {

                dispatch(updateErrorMsgCreateNewDashboard(true));
                console.log("something went wrong")
            }
        })
    }
}

export const {
    updateDashboardList,
    updateShareWithMeList,
    updateMemberList,
    updateSearchInput,
    updateShareWithOtherList,
    updateNameForDashboard,
    updateErrorMsgCreateNewDashboard,
    updateErrorMsgNoName,
    updateCreateDashboardSuccess,
    updateShareWithOtherListEmpty,
    updateDeleteOneDashboard,
    logoutDashboardSlice
    

} = dashboardSlice.actions;

export default dashboardSlice.reducer;