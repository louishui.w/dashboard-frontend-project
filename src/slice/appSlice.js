import { createSlice } from "@reduxjs/toolkit";
import { useSelector, useDispatch } from "react-redux";


const initialState = {
    isLoggedIn: localStorage.getItem("token") ? true : false,
    message: "",
    loginFail:false,
    userIdLogin: "",
    passwordLogin: "",
    registerFail: false

}

export const appSlice = createSlice({
    name: "app",
    initialState: initialState,
    reducers: {
        logoutAppSlice: function(state, action){
            state = {...initialState};
        },
        updateIsLoggedIn: function (state, action) {
            state.isLoggedIn = action.payload
        },
        updateUserIdLogin: function (state, action) {
            state.userIdLogin = action.payload
        },
        updateUserIdRegister: function (state, action) {
            state.userIdRegister = action.payload
        },
        updatePasswordLogin: function (state, action) {
            state.passwordLogin = action.payload
        },
        updatePasswordRegister: function (state, action) {
            state.passwordRegister = action.payload
        },
        updateLoginFail: function (state, action){
            state.loginFail = action.payload
        },
        updateRegisterFail: function (state, action){
            state.registerFail = action.payload
        }
        
    }
})

export const {
    updateUserIdLogin,
    updateUserIdRegister,
    updatePasswordLogin,
    updatePasswordRegister,
    updateIsLoggedIn,
    updateLoginFail,
    logoutAppSlice,
    updateRegisterFail } = appSlice.actions;

export const toLogin = () => {
    return (dispatch, getState) => {

        // console.log("have a look of the login info first:", loginInfo);
        let loginInfo = {
            username: getState().appSlice.userIdLogin,
            password: getState().appSlice.passwordLogin
        }
        fetch("/login", {
            body: JSON.stringify(loginInfo),
            headers: {
                'content-type': 'application/json'
            },
            method: 'POST'
        }).then((res) => { return res.json() }).then((resData) => {
            console.log(resData);
            if (resData.success) {
                console.log("login success!");
                console.log(resData.token);
                dispatch(updateIsLoggedIn(true));
                dispatch(updateLoginFail(false));

                localStorage.setItem("token", resData.token);
                localStorage.setItem("username", resData.username);
                dispatch(updateUserIdLogin(""));
                dispatch(updatePasswordLogin(""));


                // localStorage.setItem("userID", JSON.stringify(state.assigmentReducer.userIdLogin));
                // setLoginId("");
                // setLoginPassword("");

            } else {
                console.log("failed login")
                dispatch(updateUserIdLogin(""));
                dispatch(updatePasswordLogin(""));
                dispatch(updateLoginFail(true));
                dispatch(updateIsLoggedIn(false));

            }
        })
    }
}
export const fetchRegister = function (inputRegisterId, inputRegisterPw) {
    return async (dispatch, getState) => {


        let account = {

            username: inputRegisterId,
            password: inputRegisterPw,
        }
        let postData = {
            body: JSON.stringify(account),
            headers: {
                'content-type': 'application/json'
            },
            method: "POST"
        }

        const res = await fetch("/register", postData)
        const data = await res.json()


        if (data.message) {
            console.log("success", data)
            localStorage.setItem("token", data.token);
            localStorage.setItem("username", data.username);

            dispatch(updateIsLoggedIn(true));
            dispatch(updateRegisterFail(false));

        } else {
            console.log(data.message);
            console.log(data.reason);
            console.log(data);
            dispatch(updateRegisterFail(true));

        }

    }
}

export default appSlice.reducer;
