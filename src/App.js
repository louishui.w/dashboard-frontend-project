import './App.css';
import React, { useEffect, useState } from "react";
import {
        BrowserRouter,
        Routes,
        Route,
        Link,
        useParams,
        useOutlet,
        Navigate,
        Outlet,
        useLocation
} from "react-router-dom";
import HomePage from './components/HomePage';
import CreatedNewDshboard from "./components/CreatedNewDashboard";
import Protected from './components/Protected';
import Dashboard from './components/Dasbboard';
import { remainLoggedIn } from "./slice/appSlice";
import { useSelector, useDispatch } from "react-redux";
import CreateChart from './components/CreateChart';
import Charts from './components/Charts'





function App() {


        return (
                <BrowserRouter>
                        <Routes>
                                <Route path="/" element={<HomePage />} />
                                <Route path="dashboard" element=
                                        {<Protected> <Dashboard /></Protected>}>
                                        <Route path=":dashboardId" element={<Charts />} />
                                        <Route path=":dashboardId/create-chart"
                                                element={<CreateChart />} />
                                        <Route path="/dashboard/create"
                                                element={<CreatedNewDshboard />} />
                                </Route>


                                {/* here to start code , code your new components <CreateChart/> */}

                                {/* if you finish CreateChart, handle isLogged ?  */}


                        </Routes>
                </BrowserRouter>
        );
}

export default App;

// {/* <Route path="/dashboard" element={
//   <Protected>
//   <Dashboard/>
//   </Protected>
// }> */}
// {/* <Route path=":dashboardId" /> */}
// {/* </Route> */}
// {/* <Route path="/" element={<Navigate to="/"/>}/> */}